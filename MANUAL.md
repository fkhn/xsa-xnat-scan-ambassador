# User Manual

## Required Information to query XNAT

* The **address** of your XNAT-Server
* Your **Username** and **Password**
* A **Root-Type**
* A **Query** which asks for some fields of the chosen Root-Type
  * the following definitions are adapted from the [pyxnat-Documentation](https://pythonhosted.org/pyxnat/features/search_engine.html):
  * A ***query*** is an unordered list that contains
    * 1 or more ***constraint(s)***
    * 0 or more ***sub-queries*** (lists as this one)
    * 1 ***logical comparison method*** (`AND` or `OR`) with which the constraints are connected
  * A ***constraint*** is an ordered tuple that contains
    * 1 valid ***searchable_type/searchable_field***
    * 1 ***operator among*** `=, <, >, <=, >=, LIKE`
* A list of **Fields** you want to get back from the server

## How to work with the GUI

### <a name="overview"></a>GUI Overview

![alt text](images/GUI_overview.png)

* *Elements for Query-Creation:* (red) 1-5
* *Elements for Downloading:* (green) 6,7
* *Elements of the Diagram:* (blue) 8-10


1. Buttons for saving/loading Queries
2. The buttons 'Server Settings' and 'Send Query'
3. Dropdownbox for choosing the Root-Type
4. Query-builder used for creating the query-constraints
5. List of all defined fields of chosen Root-Type, checked fields will be requested and shown in the result-table
6. Buttons for result-table (Toggle Selection and Download selected Items (disabled if download is not defined for this Root-Type (see Root-Types: Structure of Root-Types)))
7. Result-table
8. Dropdownbox for choosing the field that will be shown in the diagram
9. List for entering search-strings, which will be shown in the diagram
10. Diagram
11. Statusbar, shows result-count (if applicable)

### Creating a Query

1. Choose a Root-Type using the Dropdownbox (see [GUI Overview](#overview): 3)
  * Root-Types are defined as JSON-files, located at `../datatypes` (see Root-Types)
2. Create the query-constraints using the Query-builder (see [GUI Overview](#overview): 4)
  * click on `...`
  * choose a logical connective (AND/OR) or a type (depends on the chosen Root-Type)
  * you can choose a single type at the beginning, to create a very simple query
  * you can nest operations and types
  * if you choose a type, make sure you choose an operator and a value too
  * you can **use % as a wildcard** if your operator is `LIKE`
  * **delete a line (and its children) by using right-click on that element** (non-reversible)
3. Choose some fields for your query using the "Result-Field"-list (see [GUI Overview](#overview): 5)
  * there are some mandatory fields you can't deselect (These are important for downloading files for this Root-Type)
  * if you don't choose any field, the server will serve all fields of this type as a response
  * choose those fields, you want to use in the diagram

Example Query:

![alt-text](images/query_example.png)

### Sending a Query

1. Click on the button "Server Settings" (see [GUI Overview](#overview): 2)
  * type your username, password and host-address ( **including protocol** , like http, https)
  * this can be set at the start by running the gui with these parameters:
  ``python xsa_gui.py --user USERNAME:PASSWORD --host http://HOST``
2. Click on the button "Send Query" (see [GUI Overview](#overview): 2)
  * if no error occurs, the results will be shown in the Result-Table (see [GUI Overview](#overview): 7) and the number of results will be shown in the statusbar (see [GUI Overview](#overview): 11)

### Downloading Files

1. Select some results in the Result-Table (see [GUI Overview](#overview): 7)
  * the "Toggle-Selection"-Button can be used for convenience (see [GUI Overview](#overview): 6)
2. Click the "Download Selected Items"-Button (see [GUI Overview](#overview): 6)
  * A window for choosing a destination-folder will appear
  * A progress indicator will appear in every selected element's row in the result-table, at the beginning of the download and disappears when the download is finished

### Using the Diagram

1. Choose one of your requested fields by using the Dropdownbox (see [GUI Overview](#overview): 8)
2. Add some search-strings to the "Chart-Value"-List (see [GUI Overview](#overview): 9) by clicking on `...`
  * **delete search-strings by right-clicking on them**
  * search-strings are not case-sensitive
  * the diagram will update immediately

Example Diagram:

![alt-text](images/diagram_example.png)

## Root-Types
### Introduction

This program needs predefined root-types to use them in queries. These Root-Types are saved as JSON-Files in the **datatypes-subfolder** in the program directory. If a file isn't correct, it will not be shown in the Root-Type-Dropdownbox. The names of these files are not important, but you have to create a correct JSON-Object, including the fields as defined in the next chapter. Only one type can be defined in a single file.

You can use the XNATRestClient to get the 	necessary information about the Root-Type you want to define. You can download this software [here](https://wiki.xnat.org/display/XNAT/XNATRestClient).
How to use this program is described in the following chapter.

### Structure of Root-Types

A Root-Type is defined by three fields:

#### "root-type"

  * Type: string
  * Description: name of the root-type you want to define for your search
  * Request types for your host by using the XNATRestClient: `XNATRestClient -host http://yourHOST -u yourUSER -p yourPASSWORD -remote "/data/search/elements?format=json" -m GET`

#### "REST-API" (not required)
  * Type: string
  * Description: REST-Interface (see the [related Documentation](https://wiki.xnat.org/display/XNAT16/Downloading+Files+via+XNAT+REST+API))
  * Write keys (see ["fields"](#fields)) of required fields in `{}`
  * If this field is not specified, the Download-button will be disabled for this Root-Type

#### <a name="fields"></a>"fields"
  * Type: list
  * Description: a list of fields for this type
  * Request the fields of a type (for example xnat:mrSessionData) by using the XNATRestClient: `XNATRestClient -host http://yourHOST -u yourUSER -p yourPASSWORD -remote "/data/search/elements/xnat:mrSessionData?format=json" -m GET`
  * Every field-definition must contain these properties:
    * `"label"`: string, just for the GUI
    * `"field"`: string, you get it from the XNATRestClient. It has to have this form: `element_name` + `/` + `id` (`element_name` is the name given by the XNATRestClient, it's actually the `root-type`)
    * `"required"`: `true`/`false`, if `true`: this field can't be deselected. Set to `true`, if this field is necessary for downloading via the defined REST-Interface
    * `"key"`: string, mainly to show the label at the result table and the diagram-dropdownbox, if not set, the `"key"` will be shown in the result-table and the diagram-dropdownbox. Usually the key is just the `"field"`-name in lower case and without `root-type/`

Example:

```json
{
"root-type":"xnat:mrScanData",
"REST-API":"/data/archive/projects/{xnat_mrsessiondata_project}/subjects/{xnat_mrsessiondata_subject_id}/experiments/{xnat_mrsessiondata_session_id}/scans/{id}/resources/DICOM/files?format=zip",
"fields":[
{"label":"Type", "field":"xnat:mrScanData/TYPE", "required":true, "key": "type"},
{"label":"Project-ID", "field":"xnat:mrSessionData/PROJECT", "required":true, "key":"xnat_mrsessiondata_project"}
]
}
```
If you neet more examples, there are some functioning Root-Types at the `./datatypes` folder ( *xnat:mrScanData*, *xnat:mrSessionData*, *xnat:projectData*, *xnat:subjectData*).


### Trouble-Shooting

#### My new Root-Type is not available in the Root-Type-Dropdownbox!

There is a mistake in your Root-Type-File.
Common Errors are:
* You ended the `"fields"`-list with a comma: ,
* You forgot a comma somewhere

If you can't figure out whats the problem, you can use an editor with json-code highlighting (like atom or gedit) or an online json-validator (http://jsonlint.com/).
