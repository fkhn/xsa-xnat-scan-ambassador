from gi.repository import Gtk

def create_error_message(message):
    """Creates a message-dialog for errors, which contains a warning-sign and a ok-button

    **Parameters**
        :message: str: message, that will be displayed
    """
    md = Gtk.MessageDialog( type=Gtk.MessageType.WARNING,
                            buttons=Gtk.ButtonsType.OK,
                            message_format=str(message)
                            )
    md.run()
    md.destroy()

def create_confirmation_message(message):
    """Creates a message-dialog with confirmation buttons (yes/no), that contains a warning-sign

    **Parameters**
        :message: str: message, that will be displayed
    """
    md = Gtk.MessageDialog(type=Gtk.MessageType.WARNING,
                           buttons=Gtk.ButtonsType.YES_NO,
                           message_format=message
                           )
    response = md.run()
    md.destroy()
    if response == Gtk.ResponseType.YES:
        return True
    return False
