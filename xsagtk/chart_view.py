"""

:Author: Franziska Koehn
:Created: 2015/01/13

This module houses classes relating to the GUI-representation of the chart.

"""

from gi.repository import Gtk
from gi.repository import GObject

class ChartView(Gtk.HPaned):
    """Container with all widgets for working with the chart."""
    results = []
    bar_chart = None
    canvas = None

    def __init__(self):

        """Creates chart-area."""

        super(ChartView, self).__init__()

        self.set_position(250)

        v_box = Gtk.VBox()
        self.add1(v_box)

        hBox_type = Gtk.HBox()
        v_box.pack_start(hBox_type, False, True, 0)

        label_type = Gtk.Label()
        label_type.set_text("Type: ")
        hBox_type.pack_start(label_type, False, True, 0)

        self.combobox = Gtk.ComboBox.new_with_model_and_entry(Gtk.ListStore(str, str))
        self.combobox.set_entry_text_column(0)
        hBox_type.pack_start(self.combobox, True, True, 0)


        sw_hist_values = Gtk.ScrolledWindow()
        v_box.pack_start(sw_hist_values, True, True, 0)

        self.TreeViewChartValues = TreeViewChartValues()

        sw_hist_values.add(self.TreeViewChartValues)

        changed_cb = lambda *_: self.update_chart()
        self.combobox.connect('changed',  changed_cb)
        self.TreeViewChartValues.connect("values-changed", changed_cb)

        self.update_chart()

        from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
        self.canvas = FigureCanvas(self.bar_chart)
        self.add2(self.canvas)


    def update_chart(self):
        """Creates the chart from all values from TreeViewChartValues"""

        import xsa.chart as chart
        substrings = []
        strings = []

        it = self.combobox.get_active_iter()
        if it is not None:
            field = self.combobox.get_model()[it][1]
            for r in self.results:
                strings.append(r[field])
        for sub in self.TreeViewChartValues.store:
            if not sub[0] == self.TreeViewChartValues.inital_value:
                substrings.append(sub[0])
        data = chart.count_substrings(substrings, strings)

        self.bar_chart = chart.create(data,len(self.results), len(self.TreeViewChartValues.store)-1, self.bar_chart)

        if self.canvas is not None:
            self.canvas.draw()

    def update(self, data, root_type):
        """
        Updates chart and combobox of types for given Root-Type and results.
        **Parameters**

            :data: JsonTable, results of search
            :root_type: str, Root-Type of search
        """
        self.results = data

        import xsa.datatypereader as type_reader

        store = Gtk.ListStore(str, str)
        if data != []:
            for key in data.headers():
                label = type_reader.get_field_label_by_key(root_type, key)
                store.append([label, key])

        self.combobox.set_model(store)
        self.combobox.set_active(0)
        self.update_chart()


class TreeViewChartValues(Gtk.TreeView, GObject.GObject):
    """TreeView for creating the Search-Strings, shown in the chart."""

    inital_value = "..."
    """Start-string for new inserted row."""

    tooltip = 'Delete by using right click'
    """Tooltip that will be shown for each row and each column"""

    __gsignals__= {'values-changed': (GObject.SIGNAL_RUN_FIRST, None,())}

    def __init__(self):
        """Creats the model and column."""

        super(TreeViewChartValues, self).__init__()
        self.create_model()
        self.create_column()



        def on_treeview_button_press_event(treeview, event):
            if event.button == 3:
                x = int(event.x)
                y = int(event.y)
                path_info = treeview.get_path_at_pos(x, y)
                if path_info is not None:
                    path, col, cellx, celly = path_info
                    iter = self.store.get_iter(path)
                    if self.store.get_value(iter,0) != self.inital_value:
                        self.store.remove(iter)
            self.emit("values-changed")

        self.connect('button_press_event', on_treeview_button_press_event)
        self.set_tooltip_column(1)


    def create_column(self):
        """Creates column and its edited-callback."""

        def cell_edited_callback(cellrenderertext, path_string, new_text, *_):
            it = self.store.get_iter_from_string(path_string)
            it_last = self.store.get_iter(len(self.store)-1)
            self.store.set(it, 0, new_text)
            if not (it_last is None) and (self.store.get_value(it_last, 0) != self.inital_value):
                self.store.append([self.inital_value, self.tooltip])
            self.emit("values-changed")


        rendererText = Gtk.CellRendererText()
        rendererText.set_property('editable', True)
        rendererText.connect('edited', cell_edited_callback, 2)

        column = Gtk.TreeViewColumn("Chart Values (Type Substrings)", rendererText, text=0)
        column.set_sort_column_id(0)
        self.append_column(column)

    def create_model(self):
        """creates store and set it as model."""
        self.store = Gtk.ListStore(str, str) # Search-String, tooltip
        self.store.append([self.inital_value, self.tooltip])
        self.set_model(self.store)
