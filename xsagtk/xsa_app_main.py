"""

:Author: Franziska Koehn
:Created: 2015/01/13

This module contains the window-class and the method for starting the GUI.

"""

from gi.repository import Gtk

class XsaApp(Gtk.Window):
    """Main Window of xsa"""

    args = None
    """Arguments from command-line"""

    def init_connection_fields(self):
        """Sets args (given Command-Line-Arguments) to their fields"""
        self.controller.host = self.args.host
        self.controller.credentials = self.args.user

    def __init__(self, args):
        """
        Creates the Window and its views. Initializes xsagtk.main_controller.

        **Parameters**
            :args: Namespace, Command-Line-Arguments from argparse
        """
        super(XsaApp, self).__init__()

        self.args = args

        self.set_size_request(1000, 500)
        #self.set_position(Gtk.WIN_POS_CENTER)
        self.connect("destroy", Gtk.main_quit)
        self.set_title("xsa")

        def on_delete_event(*_):
            "Goodby!"

        self.connect("delete-event", on_delete_event)

        # vbox_root = Gtk.VBox(False, 1)
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.add(box)

        # Toolbar

        from xsagtk.menu_view import MenuView
        self.menu = MenuView()
        box.pack_start(self.menu, False, False, 0)

        # Body

        paned = Gtk.Paned(orientation=Gtk.Orientation.HORIZONTAL)
        paned.set_position(400)
        box.pack_start(paned, True, True, 5)
        # self.add(paned)

        # left side

        from xsagtk.query_view import QueryView
        self.queryview = QueryView(homogeneous=False, spacing=1)
        paned.add1(self.queryview)

        # right site

        vpaned = Gtk.VPaned()
        vpaned.set_position(200)
        paned.add2(vpaned)

        # results-table

        from xsagtk.results_view import ResultsDownloadView
        self.resultsview = ResultsDownloadView(False,1)
        vpaned.add1(self.resultsview)

        # chart
        from xsagtk.chart_view import ChartView
        self.chartview = ChartView()
        vpaned.add2(self.chartview)

        # statusbar

        self.statusbar = Gtk.Statusbar()
        box.pack_start(self.statusbar, False, False, 0)

        from xsagtk.main_controller import QueryController
        self.controller = QueryController(self, self.queryview, self.menu, self.chartview, self.resultsview, self.statusbar)

        self.init_connection_fields()

        self.show_all()

def start_xsa_gui(args):
    """
    Creates the Window and passes it all CommandLine-Arguments.

    **Parameters**
        :args: Namespace, Command-Line-Arguments from argparse
    """
    XsaApp(args)
    #Gtk.gdk.threads_init() TODO
    Gtk.main()
