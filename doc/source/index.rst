.. xsa documentation master file, created by
   sphinx-quickstart on Thu Jan  8 23:13:29 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome
=======

Welcome to xsa's documentation!
This documentation is about the programmed structure of xsa.
if you want to read, how to work with XSA as a user, see the MANUAL.
If you want to know, how to install the prerequisites and how to start xsa, see the README.

Introduction
============

short readme
auf skripte und manual verweisen


Modules
=======

.. toctree::
   :maxdepth: 2

   xsagtk-module
   xsa-module


Indices and Tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
