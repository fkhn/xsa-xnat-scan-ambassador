"""

:Author: Franziska Koehn
:Created: 2015/01/13

This module contains the MenuView (a ToolBar).

"""

from gi.repository import Gtk
from gi.repository import GObject

class MenuView(Gtk.Toolbar):
    """A ToolBar containing Buttons"""

    __gsignals__ = {
        'spawn-connection-dialog': (GObject.SIGNAL_RUN_FIRST, None,()),
        'get-additional-data': (GObject.SIGNAL_RUN_FIRST, None,()),
        "send-query": (GObject.SIGNAL_RUN_FIRST, None,()),
        "toggle-selection": (GObject.SIGNAL_RUN_FIRST, None,()),
        "download-selection": (GObject.SIGNAL_RUN_FIRST, None,()),
        "save-query": (GObject.SIGNAL_RUN_FIRST, None,()),
        "load-query": (GObject.SIGNAL_RUN_FIRST, None,()),
        "export-csv": (GObject.SIGNAL_RUN_FIRST, None,())
    }

    def __init__(self, *args, **kwargs):
        """Creates buttons for this Toolbar and defines signals when this buttons will be pressed.

        **Parameters**

            :\*args: arguments passed to Gtk.Toolbar-constructor
            :\*\*kwargs: keyword-arguments passed to Gtk.Toolbar-constructor
        """
        super(MenuView, self).__init__(*args, **kwargs)

        #self.set_orientation(Gtk.ORIENTATION_HORIZONTAL)
        self.set_style(Gtk.ToolbarStyle.BOTH)


        tb_savequery = Gtk.ToolButton()
        tb_savequery.set_label("Save Query")
        tb_savequery.set_tooltip_text("Save Query")
        tb_savequery.set_stock_id(Gtk.STOCK_FLOPPY)
        tb_savequery.connect("clicked",lambda *_: self.emit("save-query"))
        self.insert(tb_savequery, 0)

        tb_loadquery = Gtk.ToolButton()
        tb_loadquery.set_label("Load Query")
        tb_loadquery.set_tooltip_text("Load Query")
        tb_loadquery.set_stock_id(Gtk.STOCK_DIRECTORY)
        tb_loadquery.connect("clicked",lambda *_: self.emit("load-query"))
        self.insert(tb_loadquery, 1)

        tb_servset = Gtk.ToolButton()
        tb_servset.set_label("Server Settings")
        tb_servset.set_tooltip_text("Alter host, user-name and password")
        tb_servset.set_stock_id(Gtk.STOCK_DIALOG_AUTHENTICATION)
        tb_servset.connect("clicked",lambda *_: self.emit("spawn-connection-dialog"))
        self.insert(tb_servset, 2)

        tb_sendquery = Gtk.ToolButton()
        tb_sendquery.set_label("Send Query")
        tb_sendquery.set_tooltip_text("Send Query")
        tb_sendquery.set_stock_id(Gtk.STOCK_YES)
        tb_sendquery.connect("clicked",lambda *_: self.emit("send-query"))
        self.insert(tb_sendquery, 3)

        self.insert(Gtk.SeparatorToolItem(), 4)

        self.tb_toggsel = Gtk.ToolButton()
        self.tb_toggsel.set_label("Toggle Selection")
        self.tb_toggsel.set_tooltip_text("Toggle selection of result-table")
        self.tb_toggsel.set_stock_id(Gtk.STOCK_SELECT_ALL)
        self.tb_toggsel.connect("clicked",lambda *_: self.emit("toggle-selection"))
        self.insert(self.tb_toggsel, 5)

        self.tb_downl = Gtk.ToolButton()
        self.tb_downl.set_label("Download Selected Items")
        self.tb_downl.set_stock_id(Gtk.STOCK_SAVE)
        self.tb_downl.connect("clicked",lambda *_: self.emit("download-selection"))
        self.insert(self.tb_downl, 6)

        self.tb_export = Gtk.ToolButton()
        self.tb_export.set_label("Export As CSV")
        self.tb_export.set_stock_id(Gtk.STOCK_CONVERT)
        self.tb_export.connect("clicked",lambda *_: self.emit("export-csv"))
        self.insert(self.tb_export, 7)

        self.insert(Gtk.SeparatorToolItem(), 8)

        self.tb_adddata = Gtk.ToolButton()
        self.tb_adddata.set_label("Get Additional Data")
        self.tb_adddata.set_stock_id(Gtk.STOCK_INDEX)
        self.tb_adddata.connect("clicked",lambda *_: self.emit("get-additional-data"))
        self.insert(self.tb_adddata, 9)

        self.disable_download_button(False, "No Results")
        self.disable_adddata_button(False, "No Results")
        self.disable_export_button(False, "No Results")


    def disable_export_button(self, is_enable, text):
        """Disables export-button and sets given text as tooltip.

        **Parameters**
            :is_enable: bool: true for enabling and false for disabling export-button
            :text: str, that will be set as tooltip
        """
        self.tb_export.set_tooltip_text(text)
        self.tb_export.set_sensitive(is_enable)

    def disable_download_button(self, is_enable, text):
        """Disables download-button and sets given text as tooltip.

        **Parameters**
            :is_enable: bool: true for enabling and false for disabling download-button
            :text: str, that will be set as tooltip
        """
        self.tb_toggsel.set_tooltip_text(text)
        self.tb_toggsel.set_sensitive(is_enable)

        self.tb_downl.set_tooltip_text(text)
        self.tb_downl.set_sensitive(is_enable)

    def disable_adddata_button(self, is_enable, text):
        """Disables get-additional-data-button and sets given text as tooltip.

        **Parameters**
            :is_enable: bool: true for enabling and false for disabling button
            :text: str, that will be set as tooltip
        """
        self.tb_adddata.set_tooltip_text(text)
        self.tb_adddata.set_sensitive(is_enable)
