#!/usr/bin/env python
import xsa.queries as queries

def main(host, user, passw):
    import distutils.core

    #TODO Read query from json-file

    root_element = 'xnat:mrScanData'
    constraints = [
        ('xnat:mrScanData/TYPE', 'LIKE', '%t2%'),
        ('xnat:mrScanData/PARAMETERS_FLIP', '>=', '10'),
        'AND',
        [('xnat:mrScanData/PARAMETERS_TE', '>', '2.0'),
         ('xnat:mrScanData/PARAMETERS_TE', '<', '2.0'),
         'OR'
         ]
        ]

    search_fields=[ 'xnat:mrScanData/TYPE',
                    'xnat:mrSessionData/PROJECT',
                    'xnat:mrSessionData/SUBJECT_ID',
                    'xnat:mrSessionData/SESSION_ID',
                    'xnat:mrScanData/ID'
                    ]
    rest="/data/archive/projects/{xnat_mrsessiondata_project}/subjects/{xnat_mrsessiondata_subject_id}/experiments/{xnat_mrsessiondata_session_id}/scans/{id}/resources/DICOM/files?format=zip"


    results = queries.search_for(host, root_element, constraints,search_fields, user, passw)

    print "Search results (%s):" % len(results)
    print results

    while True:
        try:
            is_downl = distutils.util.strtobool(raw_input("download? "))
            break
        except ValueError:
            print "invalid character. Please use y or n."

    if is_downl:
        print "downloading files..."
        for result in results:
            print result
            queries.download_async(result, host, (user, passw), rest, dest_folder='')
        print "finished"
    else:
        print "download canceled"

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='search and download')
    parser.add_argument('--host', type=str, help='hostname or ip-address (including port).', default="localhost:8080")
    parser.add_argument('--user', type=str, help='user:passw', default=":")

    args = parser.parse_args()

    u,p = args.user.split(':')
    main(args.host, u, p)
