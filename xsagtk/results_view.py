"""

:Author: Franziska Koehn
:Created: 2015/01/13

This module houses classes relating to the results-view.

"""

from gi.repository import Gtk

class ResultsDownloadView(Gtk.Box):
    """Container, that contains all Result-View-widgets."""

    def toggle_selection(self, *_):
        """Toggles Selection in table"""
        self.treeViewResults.toggle_selection()

    def get_selected_items(self):
        """Returns selected items of TreeViewResults"""
        return list(r for r in self.get_store() if r[0]) # r[0] = if selected

    def get_store(self):
        """Returns store of TreeViewResults"""
        return self.treeViewResults.store

    def show_data(self, results, root_type):
        """
        Shows results in TreeViewResults.

        **Parameters**

            :results: JsonTable, results of search
            :root_type: str, Root-Type of search
        """
        self.treeViewResults.show_data(results, root_type)

    def __init__(self, *args, **kwargs):
        """Creates Result-View (containing the TreeViewResults)

        **Parameters**

            :\*args: arguments passed to Gtk.VBox-constructor
            :\*\*kwargs: keyword-arguments passed to Gtk.VBox-constructor
        """
        super(ResultsDownloadView, self).__init__(*args, **kwargs)

        sw_results = Gtk.ScrolledWindow()
        self.pack_start(sw_results,  True, True, 0)

        self.treeViewResults = TreeViewResults()
        sw_results.add(self.treeViewResults)


class TreeViewResults(Gtk.TreeView):
    """TreeView which shows Results of search."""

    store = ()
    """Gtk.TreeStore, includes results of search"""

    def __init__(self):
        """Calls parent-constructor, calls show_data() with empty list"""
        super(TreeViewResults, self).__init__()
        self.show_data([], None)
        from gi.repository import GObject
        GObject.timeout_add(150, self.move_spinners)
        # sets self.move_spinners to be called at regular intervals (150)

    def move_spinners(self):
        """Moves Circles in TreeViewResult (while downloading, gets called periodically)."""
        for r in self.store:
            if r[2]: # if spinner is visible
                r[3] += 1
        return True


    def create_columns(self, column_labels):
        """
        Creates columns for every entry in column_labels.

        **Parameters**

            :column_labels: list of str, labels of columns
        """

        for c in self.get_columns():
            self.remove_column(c)

        def callback_toggled(cellrenderertoggle, path_string, col, *_):
            it = self.store.get_iter_from_string(path_string)
            is_active = cellrenderertoggle.get_active()
            self.store.set(it, col, not is_active)

        renderer = Gtk.CellRendererToggle()
        renderer.set_property('activatable', True)
        renderer.connect("toggled", callback_toggled, 0)
        column = Gtk.TreeViewColumn("", renderer)
        column.add_attribute(renderer, "active", 0)
        column.set_sort_column_id(0)
        column.set_resizable(False)
        self.append_column(column)

        renderer = Gtk.CellRendererSpinner()
        column = Gtk.TreeViewColumn("", renderer, active=2, pulse=3)
        self.append_column(column)

        for i,cl in enumerate(column_labels, start=4):
            column = Gtk.TreeViewColumn(cl, Gtk.CellRendererText(), text=i)
            column.set_sort_column_id(i)
            column.set_resizable(True)
            self.append_column(column)

    def toggle_selection(self):
        """Toggles (inverts) Selection of TreeViewResult"""
        for row in self.store:
            row[0] = not row[0]

    def show_data(self, data, root_type):
        """
        Shows given data in TreeViewResults.

        **Parameters**

            :data: JsonTable, results of search
            :root_type: str, Root-Type of search
        """
        from gi.repository import GObject

        if data == []:
            if self.store != ():
                self.store.clear()
            self.create_columns([])
            return

        keys = data.headers()

        model_columns = []
        model_columns.append(bool)                  # 0 is selected (for download selection)
        model_columns.append(GObject.TYPE_PYOBJECT) # 1 query result
        model_columns.append(bool)                  # 2 is spinner visible
        model_columns.append(int)                   # 3 spinner pulse

        for k in keys:
            model_columns.append(str)   # add a column for every key (=column in result-set) to store

        self.store = Gtk.ListStore(*model_columns)

        for d in data:      # add a row for every result
            result = []
            result.append(True)
            result.append(d)
            result.append(False)
            result.append(0)
            for k in keys:
                result.append(d[k])

            self.store.append(result)

        import xsa.datatypereader as type_reader
        keys_ = list(type_reader.get_field_label_by_key(root_type, key) for key in keys)
        self.create_columns(keys_)
        # create a column for every key (use as label for the column headers the label)

        self.set_model(self.store)
